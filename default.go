// Copyright 2016 Thomas Lingefelt. All rights reserved.
// Use of this source code is governed by a ISC-style
// license that can be found in the LICENSE file.

package simpleclient

// This file is for functions relating to the default client.

import (
	"os"
)

type ProgHandFunc func(part, total int64)

// The client that's used by the default functions.
var client = NewSimpleClient()

// The progress handler used by the default's fucntions.
var ProgressHandler ProgHandFunc = DefaultProgressHandler

// Call this to get a fresh new client
func ResetClient() {
	client = NewSimpleClient()
}

// Get a url
func Get(url string) ([]byte, error) {
	return client.Get(url)
}

// Get a URL with progress
func ProGet(url string) ([]byte, error) {
	return client.ProGet(url)
}

// Returns the default client's LastUrl
func LastUrl() string {
	return client.LastUrl
}

// Set the last URL and on the next request send it in the
// Referer header
func SetLastUrl(url string) {
	client.LastUrl = url
	client.UseLastUrl = true
}

func SetHeaders(h map[string]string) {
	client.Headers = h
}

// Common code for the Download and ProDown functions.
func dlHelper(url, foutName string, progFunc ProgHandFunc) error {
	fout, err := os.Create(foutName)
	if err != nil {
		return err
	}
	defer fout.Close()
	return client.Dl(url, fout, progFunc)
}

// Downloads a URL to a file
func Download(url, foutname string) error {
	return dlHelper(url, foutname, nil)
}

// Downloads a URL to a file with a progress bar
func ProDown(url, foutname string) error {
	return dlHelper(url, foutname, ProgressHandler)
}
