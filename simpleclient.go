// Copyright 2016 Thomas Lingefelt. All rights reserved.
// Use of this source code is governed by a ISC-style
// license that can be found in the LICENSE file.

/*
	This is a simple client that enables downloading of urls
	while setting the referer and agent headers.

	Can also display progress bars while downloading data.

	It's mostly just a wrapper around http.Client.

	At it's very basic usage...
		import sc "simpleclient"
		...
		data, err := simpleclient.Get(url)
*/
package simpleclient

import (
	"bytes"
	"io"
	"net/http"
	"os"
	"strconv"
	"time"
)

// The SimpleClient.
type SimpleClient struct {
	// The AgentHeader
	Agent string
	// The last URL from a Get(url) or ProGet(url) call.
	LastUrl string
	// Whether or not to send the Referer header.
	UseLastUrl bool
	// Custom headers
	Headers map[string]string
	//The thing which is wrapped.
	client http.Client
}

// Create a new client.
func NewSimpleClient() *SimpleClient {
	return &SimpleClient{Agent: "TeerlSimpleClient/1.0"}
}

func (sc *SimpleClient) fetchResponse(url string) (*http.Response, error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("User-agent", sc.Agent)
	if sc.UseLastUrl && len(sc.LastUrl) > 0 {
		req.Header.Add("Referer", sc.LastUrl)
	}

	for key, value := range sc.Headers {
		req.Header.Set(key, value)
	}

	resp, err := sc.client.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}

// Download the url writing the data to fout and optionally
// using progHand to display the progress.
func (sc *SimpleClient) Dl(url string, fout io.Writer, progHand ProgHandFunc) error {
	resp, err := sc.fetchResponse(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	conHead := resp.Header["Content-Length"]
	var conlen int64 = 0
	if len(conHead) > 0 {
		temp, _ := strconv.Atoi(conHead[0])
		conlen = int64(temp)
	}

	var fileBuff bytes.Buffer
	var totalRead int64 = 0

	killProgress := make(chan struct{})
	if progHand != nil {
		go func() {
			defer func() { killProgress <- struct{}{} }()
			for {
				select {
				case <-time.After(time.Millisecond * 100):
					progHand(totalRead, conlen)
				case <-killProgress:
					progHand(totalRead, conlen)
					os.Stdout.Write([]byte{'\n'})
					return
				}
			}
		}()
	} else {
		go func() {
			<-killProgress
			killProgress <- struct{}{}
		}()
	}

	for err == nil {
		var readErr, writeErr error

		fileBuff.Reset()
		read, readErr := io.CopyN(&fileBuff, resp.Body, 131072)
		totalRead += read

		if readErr == nil || readErr == io.EOF {
			_, writeErr = io.CopyN(fout, &fileBuff, read)
		}

		if readErr != nil {
			err = readErr
		} else if writeErr != nil {
			err = writeErr
		}
	}
	killProgress <- struct{}{}
	<-killProgress
	if err != nil && err != io.EOF {
		return err
	}
	sc.LastUrl = url
	return nil
}

// Gets the url and returns the data
func (sc *SimpleClient) Get(url string) ([]byte, error) {
	var buff bytes.Buffer
	err := sc.Dl(url, &buff, nil)
	return buff.Bytes(), err
}

// Get() with progress. Uses the default progress handler.
func (sc *SimpleClient) ProGet(url string) ([]byte, error) {
	var buff bytes.Buffer
	err := sc.Dl(url, &buff, ProgressHandler)
	return buff.Bytes(), err
}
