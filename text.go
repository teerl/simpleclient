// Copyright 2016 Thomas Lingefelt. All rights reserved.
// Use of this source code is governed by a ISC-style
// license that can be found in the LICENSE file.

package simpleclient

// This file for funcs relating to string processing or production.

import (
	"fmt"
	"regexp"
)

// For cleaning up strings that are to used as filenames
func Sanitize(src string) string {
	white_re, _ := regexp.Compile(`[^\w]+`)
	return white_re.ReplaceAllString(src, "_")
}

// Bytes to human readable.
func PrettyByte(n int64) string {
	num := float64(n)
	unit := 1000.0
	affix := "BKMG"
	affix_c := 0
	for num > unit && affix_c < 3 {
		num /= unit
		affix_c += 1
	}
	return fmt.Sprintf("%0.2f%c", num, affix[affix_c])
}

// Make a report string for part of total bytes
func FormReport(part, total int64) string {
	if total > 0 && part <= total {
		percent := (float64(part) / float64(total)) * 100
		return fmt.Sprintf("%s/%s %0.2f%%", PrettyByte(part), PrettyByte(total), percent)
	}
	return "Read " + PrettyByte(part)
}

func DefaultProgressHandler(part, total int64) {
	fmt.Printf(defaultFormatString, FormReport(part, total))
}
